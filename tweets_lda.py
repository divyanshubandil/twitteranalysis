import re
from gensim import corpora, models
import nltk
import copy
import pyLDAvis.gensim
import sys
reload(sys) 
sys.setdefaultencoding('utf8')


class Tweet:
    def __init__(self, label, tweet, prev_tweets):
        self.label = label
        self.tweet = tweet
        self.prev_tweets = prev_tweets
        self.tweet_tokens = []
        self.prev_tweet_tokens = []

    def tokenize(self):
        tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
        self.tweet_tokens = tokenizer.tokenize(self.tweet)
        for i, tweet in enumerate(self.prev_tweets):
            self.prev_tweet_tokens.append(tokenizer.tokenize(tweet))

    def remove_stopwords(self):
        stopw = nltk.corpus.stopwords.words('english')
        self.tweet_tokens = [word for word in self.tweet_tokens if word not in stopw]
        for i, tweet in enumerate(self.prev_tweet_tokens):
            self.prev_tweet_tokens[i] = [word for word in tweet if word not in stopw]


    def cleanup(self):
        self.tweet = self.cleanup_using_regex(self.tweet)
        self.prev_tweets = self.prev_tweets.split(",")
        for i, tweet in enumerate(self.prev_tweets):
            self.prev_tweets[i] = self.cleanup_using_regex(tweet)

    def cleanup_using_regex(self, tweet):
        # Remove retweet tag
        tweet = re.sub(r"RT", " ", tweet)
        # Remove @ tags
        tweet = re.sub(r"(?:\@)\S+", " ", tweet)
        # Remove Url
        tweet = re.sub(r"(?:https?\:\/\/)\S+", " ", tweet)
        # Separate out apostrophe based short
        tweet = re.sub(r"\'s", " \'s", tweet)
        tweet = re.sub(r"\'ve", " \'ve", tweet)
        tweet = re.sub(r"n\'t", " n\'t", tweet)
        tweet = re.sub(r"\'re", " \'re", tweet)
        tweet = re.sub(r"\'d", " \'d", tweet)
        tweet = re.sub(r"\'ll", " \'ll", tweet)
        # Remove unwanted characters
        tweet = re.sub(r"[^A-Za-z]", " ", tweet)
        # Moving the tweet to lower case words
        tweet = tweet.lower()
        return tweet

    def remove_one_and_two_letter_words(self):
        self.tweet_tokens = [x for x in self.tweet_tokens if len(x) > 2]
        for i, tweet in enumerate(self.prev_tweet_tokens):
            self.prev_tweet_tokens[i] = [x for x in tweet if len(x) > 2]


class Feature:
    def __init__(self):
        pass

    def combined_tweets(self, tweets):
        documents = []
        for tweet in tweets:
            tweet_copy = tweet.tweet_tokens
            for tweet_tokens in tweet.prev_tweet_tokens:
                tweet_copy.extend(tweet_tokens)
            documents.append(tweet_copy)
        return documents

    def present_tweet(self, tweets):
        documents = []
        for tweet in tweets:
            tweet_copy = tweet.tweet_tokens
            documents.append(tweet_copy)
        return documents

    def previous_tweet(self, tweets):
        documents = []
        for tweet in tweets:
            tweet_copy = []
            for tweet_tokens in tweet.prev_tweet_tokens:
                tweet_copy.extend(tweet_tokens)
            documents.append(tweet_copy)
        return documents

    def calculate_and_remove_below_freq(self, tweets, n):
        Words = {}
        for tweet in tweets:
            for word in tweet:
                if word in Words:
                    Words[word] += 1
                else:
                    Words[word] = 1

        for i, tweet in enumerate(tweets):
            tweets[i] = [x for x in tweet if Words[x] > n]
        return tweets

def process_line(tweets, line):
    data = line.strip().split(",",18)
    tweet = Tweet(data[16].strip("\""), data[17], data[18])
    tweets.append(tweet)


def get_tweets(file):
    tweets = []
    with open(file, "rb") as f:
        lines = f.readlines()
        count = 2
        prev_line = lines[1]
        lines = lines[2:len(lines)]
        for line in lines:
            if line.startswith("\"" + str(count) + "\"") and prev_line != "":
                process_line(tweets, prev_line)
                prev_line = line
                count += 1
            else:
                prev_line += line
        if prev_line != "":
            process_line(tweets, prev_line)
    return tweets

if __name__ == "__main__":
    # Read data
    tweets = get_tweets("data/export.csv")
    # Cleanup and tokenize
    for tweet in tweets:
        tweet.cleanup()
        tweet.tokenize()
        tweet.remove_stopwords()
        tweet.remove_one_and_two_letter_words()

    features = Feature()
    # Prepare documents
    combined_tweets = features.combined_tweets(tweets)
    combined_tweets = features.calculate_and_remove_below_freq(combined_tweets, 1)

    dictionary = corpora.Dictionary(combined_tweets)
    dictionary.compactify()
    dictionary.save_as_text("modeldict.txt")

    corpus = [dictionary.doc2bow(doc) for doc in combined_tweets]
    lda = models.LdaModel(corpus, id2word=dictionary, num_topics=5, passes=20, alpha=0.001)
    lda.print_topics()
    lda.save('data/combined_model_3.save')
    # lda = models.LdaModel.load('data/combined_model_2.save')

    data =  pyLDAvis.gensim.prepare(lda, corpus, dictionary)
    pyLDAvis.display(data)